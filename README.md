# Ag-Grid Challenge

The challenge is to create a component in an Angular app that includes an ag-grid table. The
requirements for this table are:
- [X] The data can be obtained from any source, the simplest way is an array of objects defined in the
  component itself. The objects represent the desktop computers inventory of an office. The fields are:
  ID, hostname, assigned IP, date of purchase, office, operating system, GB of hard disk storage. The
  ID column does not have to be displayed directly as a column in the table.
- [X] The columns can be sorted, resized and filtered with a floating filter.
- [X] The table has to allow pagination and include a quick search box.
- [X] The information in the column "date of purchase" has to have a CSS styling applied to it to
  highlight if it is older than 4 years.
- [X] The storage column is a number value, so it is sorted and filtered as a number. But when
  displayed, it has to be formatted as "xx GB". E.g.: if the column has the value 5, it has to appear in
  the table "5 GB".
- [X] Each row includes 2 button columns. The action of button 1 is to display an alert with the
  computer ID. The action of button 2 is to delete the computer.
  Any cellStyle, valueFormatter or cellRenderer applied in a remarkable way to the rest of the
  columns will add points. For example, display an operating system icon next to the value.

## Getting started

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.0.1.

### Install dependencies
```sh
$ npm install --save ag-grid-community
```
### Start Development Server

Run `node start` for a dev server.  Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## License

This porject is licensed under the MIT license. See the [LICENSE file](./LICENSE) for more info.
