import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AgGridModule } from 'ag-grid-angular';
import {FormsModule} from "@angular/forms";
import { AppComponent } from './app.component';

import { BtnDeleteRendererComponent} from '../renderer/btn-delete-renderer';
import 'ag-grid-community'

@NgModule({
declarations: [
  AppComponent,
  BtnDeleteRendererComponent
],
imports: [
  BrowserModule,
  HttpClientModule,
  AgGridModule,
  FormsModule
],
providers: [],
bootstrap: [AppComponent]
})
export class AppModule {}
