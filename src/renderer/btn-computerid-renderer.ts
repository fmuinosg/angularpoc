import {Component} from '@angular/core';
import {ICellRendererAngularComp} from 'ag-grid-angular';
import {ICellRendererParams} from 'ag-grid-community';

@Component({
  selector: 'app-child-cell',
  template: '<button (click)="btnClickedHandler()">Click me!</button>',
})

export class BtnComputerIDRendererComponent implements ICellRendererAngularComp {
  public params!: any;

  agInit(params: any): void {
    this.params = params;
  }

  btnClickedHandler() {
    alert(`This Computer ID is ${this.params.data.ID}! `);
  }

  refresh() {
    return true;
  }

  getValueToDisplay(params: ICellRendererParams) {
    return params.valueFormatted ? params.valueFormatted : params.value;
  }

}
